import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { SearchBarComponent } from './core/search-bar/search-bar.component';
import { AppRoutingModule } from './app-routing.module';
import { WeatherComponent } from './weather/weather.component';
import { WeatherService } from './weather/service/weather.service';
import { WeatherDetailComponent } from './weather/weather-detail/weather-detail.component';
import { CityDetailComponent } from './weather/city-detail/city-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchBarComponent,
    WeatherComponent,
    WeatherDetailComponent,
    CityDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    AgmCoreModule.forRoot(
      {apiKey: "AIzaSyAhNnZ6S5SJ_JPH8GRw2yQT6wszFVYuKWA"}
    )
  ],
  providers: [
    WeatherService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
