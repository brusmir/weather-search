import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'wt-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
	private userInput;
	private cityNames: string[];
	private delCity: string = "Delete,";

  constructor(private router: Router) { }

  ngOnInit() {
  }

  //Creating a method that is called when a user submits the form
  onSubmit(){
   	let param = this.userInput;  //string
   	param = param.toLowerCase();
   	param = param.split(",");  //array
    	   	
   	//Removing empty space around the city in order to find a city in the database    	
   	param = param.map(function(value, index, array){
   		return value.trim();
   	});
    	
   	//Removing a comma after the city that is the last in a row, in order to find a city in the database 
   	//Convert the first letter to a capital letter
   	let param2 = [];
   	for(let i = 0; i < param.length; i++){
   		if(param[i][param[i].length - 1] == ","){  
     		let nov = param[i].slice(0, -1);
     		param2.push(nov.charAt(0).toUpperCase() + nov.slice(1));          		
   		}else{
        		param2.push(param[i].charAt(0).toUpperCase() + param[i].slice(1));
   		}      
   	} 
    	
    //Removing an empty space after a comma, otherwise the array would have an item that is an empty string
    for(let i = 0; i < param2.length; i++){
    	if(param2[i] == ""){                    
     		let item = param2.indexOf(param2[i]);  
     		param2.splice(item, 1);
    	}
    }
    param = param2  
    	
    //Fill in the cityNames array with cities that the user has entered and eject duplicate entries
    this.cityNames = param;  
    this.cityNames = this.cityNames.filter(function(value, index, array){
    	return array.indexOf(value) == index;          
    });

    //Limit cityNames array 
    if(this.cityNames.length > 20 ){
    	this.cityNames.length = 20;
    }
		
		//Convert an array to a string and send a string as a parameter to the Weather web page
    param = this.cityNames.join();
    this.router.navigate(['weather/', param]);
    	
    //Clearing the input field and removing the focus from the same
    let input = document.getElementsByTagName("input")[0] as HTMLInputElement;
    input.value = "";
    input.blur();      
	}

	//Create a method that will delete the label tag the user clicked on and forward the name of the city to the Weather web page
	deleteCity(name){
    this.delCity += name;
    this.router.navigate(['weather/', this.delCity]);
    let lab = document.getElementsByClassName(name)[0] as HTMLObjectElement;
    if(lab){
      lab.style.display = "none";
    }  
    this.delCity = "Delete,";     	
  }

  //Creating a method that will reset the url when a user clicks into the input field and remove label tags from a web page
	resetUrl(){
   	this.router.navigate(['']);     	
   	let labels = document.getElementsByTagName("label");
   	for(let i = 0; i < labels.length; i++){
   		labels[i].style.display = "none";      
   	}
   	this.cityNames = [];    
  }
}
