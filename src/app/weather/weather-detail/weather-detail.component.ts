import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'wt-weather-detail',
  templateUrl: './weather-detail.component.html',
  styleUrls: ['./weather-detail.component.css']
})
export class WeatherDetailComponent implements OnInit {

	@Input() private list;
	@Output() private hideLabes = new EventEmitter();

	//The command to create the current date
	private date = new Date().toISOString();

  constructor() { }

  ngOnInit() {
  }

  //The method that inform the parent component that user passed to the CityDetail web page
  onHideLabels(){
  	this.hideLabes.emit();
  }
}
