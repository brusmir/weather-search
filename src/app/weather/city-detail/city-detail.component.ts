import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../service/weather.service';
import { PerDayModel } from '../model/perday';

@Component({
  selector: 'app-city-detail',
  templateUrl: './city-detail.component.html',
  styleUrls: ['./city-detail.component.css']
})
export class CityDetailComponent implements OnInit {

	private list: PerDayModel[]; 
  private list2: PerDayModel[] = [];

  //These two properties are used to center the Google map to the city the user chooses
	private latitude;
	private longitude;	

  constructor(private weatherService: WeatherService, private route: ActivatedRoute) { }

  //Take a parameter from a url
  //Call the service method in order to getting data for the selected city
  ngOnInit() {
  	let cityId = this.route.snapshot.paramMap.get('id');
  	this.weatherService.getCity(cityId).subscribe( data => {
  		this.list = data.list;
  		this.latitude = data.city.coord.lat;
  		this.longitude = data.city.coord.lon; 
        
  		//Through the loop put out the forecast for every three hours and got one information per day
     	let date = "0";      		
     	for(let i = 0; i < this.list.length; i++){
       	let tmp = this.list[i].dt_txt.split(" ");
       	if(tmp[0] != date){
       		date = tmp[0];
       		this.list2.push(this.list[i]);
       	}
     	}      		
  	});   
  }
}

