import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { CityModel } from '../model/city-model';
import { ForecastModel } from '../model/forecast';

const url = "http://api.openweathermap.org/data/2.5/weather?q=";
const cityUrl = "http://api.openweathermap.org/data/2.5/forecast?id=";

@Injectable()
export class WeatherService {
	
  constructor(private http: HttpClient) { }

  getCities(id: string): Observable<CityModel>{
  	return this.http.get(url + id + "&units=metric&APPID=942985dc1b13e9207040b0a842e0eac9").map( 
      response => new CityModel(response)
    ).catch(this.handleError);
  }

  handleError(error: Response){
    return Observable.throw(error);
  }

  getCity(id: string):Observable<ForecastModel>{
  	return this.http.get(cityUrl + id + "&units=metric&APPID=942985dc1b13e9207040b0a842e0eac9").map( 
		  response => new ForecastModel(response)
    );
  }

}

