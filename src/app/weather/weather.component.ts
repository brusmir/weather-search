import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from './service/weather.service';
import { CityModel } from './model/city-model';

@Component({
  selector: 'wt-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  private list: Array<CityModel> = [];

  constructor(private route: ActivatedRoute, private weatherService: WeatherService) { }

  ngOnInit() {
    this.processParam();
  }

  //Creating a method that will process the parameters passed through url
  processParam(){
    this.route.params.subscribe( param => {
      let userInput = param['search']; //string   
      
      if(userInput){
        let cities = userInput.split(","); //array       

        //If the passed parameter is not a delete command:  
        //Fill out the list array with data of forwarded cities           
        //If cities can not be found, send a message to the user
        if(cities[0] != "Delete"){           
          for(let i=0; i<cities.length; i++){          
            this.weatherService.getCities(cities[i]).subscribe( respo => this.list.push(respo), 
              error =>{     
                if(i == cities.length - 1){           
                  if(this.list.length == 0){                  
                    let parent =  document.getElementsByClassName("parent")[0] as HTMLObjectElement;
                    parent.innerHTML = "<div class='offset-md-1 notFound'>Not found<div>";
                  }
                }
              }              
            ); 
          }                        
        }

        //If the passed parameter is a delete command:
        //Find the city's name
        //Removing a div whose class value corresponds to the city's name
        //If there is not the required city, deleting the div with the notFound class       
        if(cities[0] == "Delete"){              
          let parent = document.getElementsByClassName("parent")[0] as HTMLObjectElement;          
          let city:CityModel;

          //In case of unpredictable behavior, we can use the service method instead of the list array
          // this.weatherService.getCities(cities[1]).subscribe( respo => {
          //   city = respo;                                                        
          //   let child = parent.getElementsByClassName(city.name)[0] as HTMLObjectElement; 
          //   if(child){ 
          //     child.style.display = "none";    
          //   }      
          // }, error => {            
          //   let child = parent.getElementsByClassName("notFound")[0] as HTMLObjectElement;  
          //   if(child){
          //     child.style.display = "none";              
          //   }  
          // });  
          
          if(this.list){
            for(let i=0; i<this.list.length; i++){
              if(this.list[i].name.toLowerCase() == cities[1].toLowerCase()){
                city = this.list[i];
              }
            }
            if(city){
              let child = parent.getElementsByClassName(city.name)[0] as HTMLObjectElement; 
              if(child){ 
                child.style.display = "none";
              }  
            }else{
              let child = parent.getElementsByClassName("notFound")[0] as HTMLObjectElement;
              if(child){
                child.style.display = "none";
              }  
            }
          }
        } 
      }
    });      
  }

  //Create a method that will remove label tabs when user passes onto the CityDetail web page
  hideLabels(){
    let labels = document.getElementsByTagName("label");
    for(let i = 0; i < labels.length; i++){
      labels[i].style.display = "none";  
    }
  }
}
