export class CityModel {
	
	coord: object;	
	weather: Array<any>;
	base: string;
	main: object;
	visibility: number;
	wind: object;
	clouds: object;
	sys: object;
	id: number;
	name: string;
	cod: number;

	constructor(obj?: any){
		this.coord = obj && obj.coord || null;		
		this.weather = obj && obj.weather || null;		
		this.base = obj && obj.base || null;
		this.main = obj && obj.main || null;
		this.visibility = obj && obj.visibility || null;
		this.wind = obj && obj.wind || null;
		this.clouds = obj && obj.clouds || null;
		this.sys = obj && obj.sys || null;
		this.id = obj && obj.id || null;
		this.name = obj && obj.name || null;
		this.cod = obj && obj.cod || null;
	}
}