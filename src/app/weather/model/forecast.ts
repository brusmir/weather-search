import { PerDayModel } from './perday';

export class ForecastModel{

	list: PerDayModel[];
	cnt: number;
	cod: string;
	message: number;
	city: CityDetail;

	constructor(searchResult? :any){
		this.cnt = searchResult && searchResult.cnt || null;
		this.cod = searchResult && searchResult.cod || null;
		this.message = searchResult && searchResult.message || null;
		this.city = searchResult && new CityDetail(searchResult.city) || null;
		this.list = searchResult && searchResult.list.map( result => new PerDayModel(result)) || [];
	}
}

class CityDetail {

	id: number;
	name: string;
	coord: CordDetail;
	country: string;

	constructor(obj?){
		this.id = obj && obj.id || null;
		this.name = obj && obj.name || null;
		this.coord = obj && new CordDetail(obj.coord) || null;
		this.country = obj && obj.country || null;
	}
}

class CordDetail {
	
	lat: number;
	lon: number;

	constructor(obj?){
		this.lat = obj && obj.lat || null;
		this.lon = obj && obj.lon || null;
	}
}

