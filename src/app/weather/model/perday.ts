export class PerDayModel {	
	
	weather: Array<any>;	
	main: object;	
	wind: object;
	clouds: object;
	sys: object;	
	dt: number;
	dt_txt: string;

	constructor(obj?: any){
		this.weather = obj && obj.weather || null;			
		this.main = obj && obj.main || null;		
		this.wind = obj && obj.wind || null;
		this.clouds = obj && obj.clouds || null;
		this.sys = obj && obj.sys || null;		
		this.dt = obj && obj.dt || null;
		this.dt_txt = obj && obj.dt_txt || null;
	}
}