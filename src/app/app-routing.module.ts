import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchBarComponent } from './core/search-bar/search-bar.component';
import { WeatherComponent } from './weather/weather.component';
import { CityDetailComponent } from './weather/city-detail/city-detail.component';

const appRoutes: Routes = [
	{ path: '', redirectTo: '/weather', pathMatch: 'full'},
	{ path: 'weather', component: WeatherComponent},
	{ path: 'weather/:search', component: WeatherComponent},
	{ path: 'weather/:search/:id', component: CityDetailComponent}	
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes)
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }